<?php
require_once "lib/vendor/autoload.php"; // API Google library

//include( "lib/vendor/google/apiclient/src/Google/Client.php" );
//include( "lib/vendor/google/apiclient/src/Google/Service/Storage.php" );
    /**
     * Connect to Google Cloud Storage API
     */
    $client = new Google_Client();
    $client->setApplicationName("myTrailcams");
    $credential = new Google_Auth_AssertionCredentials(
        "117512706145-compute@developer.gserviceaccount.com",
        ['https://www.googleapis.com/auth/devstorage.read_write'],
        file_get_contents("apis/mytrailcameras-6d9f5b4086bc.p12")
    );
    $client->setAssertionCredentials($credential);
    if($client->getAuth()->isAccessTokenExpired()) {
        $client->getAuth()->refreshTokenWithAssertion($credential);
        // Cache the access token however you choose, getting the access token with $client->getAccessToken()
    }
    $bucket = 'mytrailcams';
    $storage = new Google_Service_Storage($client);
    $obj = new Google_Service_Storage_StorageObject();
?>