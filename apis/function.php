<?php

function DeleteImage($imgname)
{
    echo $imgname;
    try
    {
        require_once "apis/imageApi.php";

        $response = $storage->objects->delete($bucket, $imgname);
        var_dump($response);
    }
    catch(Exception $e)
    {
        print $e->getMessage();
    }
}
function GetImages()
{
    try
    {
        require_once "apis/imageApi.php";
        $response = $storage->objects->listObjects($bucket);
        foreach ($response['items'] as $element)
        {
            $file = $element->getName();
            $request = new Google_Http_Request($element->mediaLink, 'GET');
            $signed_request = $client->getAuth()->sign($request);
            $http_request = $client->getIo()->makeRequest($signed_request);
            $img = $http_request->getResponseBody();
            file_put_contents($file, $img);

            // echo '<div class="img">
			// 	<a target="_blank" href="'.$file.'">
			// 		<img src="'.$file.'" alt="'.$file.'" width="300" height="200" id="imgsrc">
			// 	</a>
			// 	<div class="desc">"'.$file.'" <input type="submit" value="Delete Image" name="deleteImage" id="MyButton" onClick="call(\''.$file.'\');"> </div>
			// </div>';
        }

    }
    catch(Exception $e)
    {
        print $e->getMessage();
    }
}
function getImageUrl($file_name){
    try{
        require_once "apis/imageApi.php";
        $object = $storage->objects->get($bucket, $file_name);
        $request = new Google_Http_Request($object['mediaLink'], 'GET');
        $signed_request = $client->getAuth()->sign($request);
        $url = $client->getIo()->makeRequest($signed_request);
        return $url;

    } catch(Exception $e){
        print $e->getMessage();
    }
    
}
function UploadImages($name, $img)
{
    try
    {
        require "imageApi.php";  //_once
        $file_name = $name;
        $obj->setName($file_name);
        $response = $storage->objects->insert(
            "mytrailcams",
            $obj,
            ['name' => $file_name, 'data' => file_get_contents($img), 'uploadType' => 'media', 'mimeType' => 'image/jpeg']
        );
        $object = $storage->objects->get($bucket, $file_name);
        //$request = new Google_Http_Request($object->mediaLink, 'GET');
        //$signed_request = $client->getAuth()->sign($request);
        //$url = $client->getIo()->makeRequest($signed_request);
        //$img = $url->getResponseBody();
        //file_put_contents($file_name, $img);
        return $object->mediaLink;
        //print_r($response);
    }
    catch(Exception $e)
    {
        print $e->getMessage();
    }
}
function GetGCSImage($GCSmedialink) {
    try
    {
        require_once "imageApi.php";
        $request = new Google_Http_Request($GCSmedialink, 'GET');

        $signed_request = $this->client->getAuth()->sign($request);
        $http_request = $this->client->getIo()->makeRequest($signed_request);

        $img = $http_request->getResponseBody();

        return $img;
    }
    catch(Exception $e)
    {
        //print $e->getMessage();
        return 0;
    }
}

?>