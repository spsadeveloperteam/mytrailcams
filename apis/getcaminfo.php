<?php

$servername = "104.198.207.18";
$username = "mytrailcams";
$password = "dexcel2018";
$dbname = "imgrecapp";

$conn = mysqli_connect($servername, $username, $password, $dbname);

if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
} 

if($_SERVER['REQUEST_METHOD'] === 'GET'){
    $cmd = $_GET['cmd'];
    $propid = $_GET['property_id'];
    if($cmd=="camname"){
        $camid = $_GET['cam_id'];
        $result = mysqli_query($conn, "select camera_name FROM imgrec_cms_camera where property_id = '$propid' and id = '$camid'");
        $row = mysqli_fetch_array($result);
        $val = $row[0];
        $unencodedArray = [
            'camname' => $val];
        echo json_encode($unencodedArray);
    } else if($cmd=="camphotos"){
        $camid = $_GET['cam_id'];
        $result = mysqli_query($conn, "select img_datetime, filepath, img_tag, cam_id, displayName, id, weather_id, status FROM imgrec_cms_media where prop_id = '$propid' and cam_id = '$camid'");  //user_id, uid
        $storeArray = Array();
        while ($row = mysqli_fetch_array($result)) {
            $storeArray[] = array("img_datetime"=>$row['img_datetime'], "filepath"=>$row['filepath'], "img_tag"=>$row['img_tag'], "cam_id"=>$row['cam_id'], "displayName"=>$row['displayName'], "id"=>$row['id'], "weather_id"=>$row['weather_id'], "status"=>$row['status']);  
        }

        $sel = mysqli_query($conn,"select windspeed, winddirection, pressure, temperature, humidity, sunrise, sunset, moonphase, ph_id, subtitle, id, imgrec_media_weathercol from imgrec_media_weather where prop_id = '$propid' and cam_id = '$camid'");
        $data = array();

        while ($row = mysqli_fetch_array($sel)) {
            $data[] = array("windspeed"=>$row['windspeed'], "winddirection"=>$row['winddirection'], "pressure"=>$row['pressure'],"temperature"=>$row['temperature'],"humidity"=>$row['humidity'],"sunrise"=>$row['sunrise'],"sunset"=>$row['sunset'],"moonphase"=>$row['moonphase'],"ph_id"=>$row['ph_id'], "subtitle"=>$row['subtitle'], "id"=>$row['id'], "imgrec_media_weathercol"=>$row['imgrec_media_weathercol']);
        }
        

        $unencodedArray = [
        'paths' => $storeArray,
        'weather' => $data];

        echo json_encode($unencodedArray);
    } else if($cmd=="allcam"){
        $result = mysqli_query($conn, "select id, camera_name, camera_latitude, camera_longitude, status FROM imgrec_cms_camera where property_id = '$propid'");
        $storeArray = Array();
        while ($row = mysqli_fetch_array($result)) {
            $storeArray[] = array("id"=>$row['id'], "camera_name"=>$row['camera_name'], "camera_latitude"=>$row['camera_latitude'], "camera_longitude"=>$row['camera_longitude'], "status"=>$row['status']);   
        }
        $unencodedArray = [
            'camdetails' => $storeArray];
        echo json_encode($unencodedArray);
    }
    
}
?>