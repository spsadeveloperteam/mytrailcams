<?php

$servername = "104.198.207.18";
$username = "mytrailcams";
$password = "dexcel2018";
$dbname = "imgrecapp";

$conn = mysqli_connect($servername, $username, $password, $dbname);

if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
} 

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
	$json = json_decode(file_get_contents('php://input'), true);

	$property_id = $json['property_id'];
	$camera_name= $json['camera_name'];
	$camera_manufacturer = $json['camera_manufacturer'];
	$camera_modelno= $json['camera_modelno'];
	$camera_latitude = $json['camera_latitude'];
	$camera_longitude = $json['camera_longitude'];
	$status = $json['status'];
	$weather_station = null;
	$createdon = date('Y-m-d H:i:s');

	// $sql = "INSERT INTO imgrec_cms_camera (property_id, camera_name, camera_manufacturer, camera_modelno,camera_latitude, camera_longitude, weather_station,status, createdon)	VALUES ('$property_id', '$camera_name', '$camera_manufacturer', '$camera_modelno','$camera_latitude','$camera_longitude', '$weather_station','$status', '$createdon')";
	$sql = "INSERT INTO imgrec_cms_camera (property_id, camera_name, camera_manufacturer, camera_modelno, camera_latitude, camera_longitude, weather_station, status, createdon) VALUES ('$property_id', '$camera_name', '$camera_manufacturer', '$camera_modelno', '$camera_latitude', '$camera_longitude', '$weather_station', '$status', '$createdon')";

	if ($conn->query($sql) === TRUE) {
	    //echo "New record created successfully";
	    $last_id = $conn->insert_id;
	    $unencodedArray = ['cam_id' => $last_id];
		echo json_encode($unencodedArray);

	} else {
	    echo "Error: " . $sql . "<br>" . $conn->error;
	}
}
else if($_SERVER['REQUEST_METHOD'] === 'GET')
{
	$propid = $_GET['property_id'];
	$sel = mysqli_query($conn,"select * from imgrec_cms_camera where property_id = '$propid'");
	$data = array();
	$total = array();
  	while ($row = mysqli_fetch_array($sel)) {
   		$data[] = array("id"=>$row['id'],"property_id"=>$row['property_id'],"camera_name"=>$row['camera_name'],"camera_manufacturer"=>$row['camera_manufacturer'],"camera_modelno"=>$row['camera_modelno'],"camera_latitude"=>$row['camera_latitude'],"camera_longitude"=>$row['camera_longitude'],"weather_station"=>$row['weather_station'],"status"=>$row['status'],"createdon"=>$row['createdon']);
	}
	for ($x = 0; $x < sizeof($data); $x++) {
		//$id = $data[$x]->id;
		$id = $data[$x]['id'];
		//error_log($id);
		$result = mysqli_query($conn, "select count(1) FROM imgrec_cms_media where prop_id = '$propid' and cam_id = '$id'");
		$row = mysqli_fetch_array($result);
		$val = $row[0];
		array_push($total, $val);
	}   
  	// $result = mysqli_query($conn, "select count(1) FROM imgrec_cms_media where prop_id = '$propid'");
    // $row = mysqli_fetch_array($result);
    //$total = $row[0];
    $unencodedArray = [
    'total_photos' => $total,
    'camdetails' => $data];
    echo json_encode($unencodedArray);
  	//echo json_encode($data);
}
else if($_SERVER['REQUEST_METHOD'] === 'PUT')
{
	$json = json_decode(file_get_contents('php://input'), true);
	$cam_id = $json['cam_id'];
	$cam_name= $json['cam_name'];
	$cam_model = $json['cam_model'];
	$cam_mf= $json['cam_mf'];
	$cam_stat = $json['cam_stat'];

	$sql_update = "UPDATE imgrec_cms_camera SET camera_name = '$cam_name', camera_manufacturer = '$cam_mf',camera_modelno = '$cam_model',status = '$cam_stat' WHERE id= '$cam_id'";
	if ($conn->query($sql_update) === TRUE) {
		echo "Record updated successfully";
	} else {
		echo "Error: ". $conn->error;
	}
}
else if($_SERVER['REQUEST_METHOD'] === 'DELETE')
{
	$json = json_decode(file_get_contents('php://input'), true);
	$cam_id = $json['cam_id'];

	$sql_delete_camera = "DELETE FROM imgrec_cms_camera WHERE id = '$cam_id'";
	$sql_delete_media = "DELETE FROM imgrec_cms_media WHERE cam_id= '$cam_id'";
	$sql_delete_weather = "DELETE FROM imgrec_media_weather WHERE cam_id= '$cam_id'";
	if ($conn->query($sql_delete_camera) === TRUE) {
		if ($conn->query($sql_delete_media) === TRUE) {
			if ($conn->query($sql_delete_weather) === TRUE) {
				echo "Camera deleted successfully.";
			} else {
				echo "Error: ". $conn->error;
			}
		} else {
			echo "Error: ". $conn->error;
		}		
	} else {
		echo "Error: ". $conn->error;
	}
}

?>