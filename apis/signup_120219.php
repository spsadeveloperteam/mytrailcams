<?php

require "vendor/autoload.php";
use \Firebase\JWT\JWT;

$servername = "104.198.207.18";
$username = "mytrailcams";
$password = "dexcel2018";
$dbname = "imgrecapp";

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);

if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
} 

$json = json_decode(file_get_contents('php://input'), true);

$user_name = $json['user_name'];
$user_password= sha1($json['user_password']);   //base64_encode
$created = date('Y-m-d H:i:s');
$last_login= date('Y-m-d H:i:s');
$last_ip = getRealIpAddr();
$user_level = 1;
$token = "";
$permissions = "";
$first_name = $json['first_name'];
$last_name = $json['last_name'];
$gender = "m";
$dob = date('Y-m-d H:i:s');
$data = "";
$plan_id = 1;
$image_uploaded = 0;
$property_added = 0;
$camera_added = 0;
$timezone = $json['timezone'];
$account_cancel_tag = "";
$status = 1;
$plan_name = "FREE";

/* Subscription Details */

$activated_at = time();
$created_at = $activated_at;
$current_term_end = strtotime("+30 day", $activated_at);
$current_term_start = $activated_at;
$next_billing_at = strtotime("+31 day", $activated_at);
$started_at = $activated_at;
$status_sub = 'active';



if($user_name && $user_password && $first_name && $last_name)
{
	$sql_sel = "select * from imgrec_cms_users where user_name = '$user_name'";
	//$result = $conn->query($sql_sel);


	if($conn->query($sql_sel)->num_rows>0)
	{
		$msg = 'User already exist!!!'
		$unencodedArray = ['msg' => $msg];
		echo json_encode($unencodedArray);
	}
	else
	{
		//$sql_getplanid = "select * from imgrec_cms_plan where plan_name = '$plan_name'";
		//$plan = $conn->query($sql_getplanid);
		//$plan_id = $plan['id'];
		
		
		$sql = "INSERT INTO imgrec_cms_users (user_name, user_password, created, last_login, last_ip, user_level, token, permissions, first_name, last_name, gender, dob, data, plan_id, image_uploaded, property_added, camera_added, timezone, account_cancel_tag, status) VALUES ('$user_name', '$user_password', '$created', '$last_login', '$last_ip', '$user_level', '$token', '$permissions', '$first_name', '$last_name', '$gender', '$dob', '$data', '$plan_id', '$image_uploaded', '$property_added', '$camera_added', '$timezone', '$account_cancel_tag', '$status')";
		
		if ($conn->query($sql) === TRUE) {
			$last_id = $conn->insert_id;

			$unencodedJsnArray = ['user_id' =>  $last_id,
                      'activated_at' => $activated_at,
                      'created_at' => $created_at,
                      'current_term_end' => $current_term_end,
                      'current_term_start' => $current_term_start,
                      'next_billing_at' => $next_billing_at,
                      'plan_id' => $plan_name,
                      'started_at' => $started_at,
                      'status' => $status_sub];

			$jsnVal = json_encode($unencodedJsnArray);

			$sql_subscrition = "INSERT INTO imgrec_ecommerce_paymentdata (user_id, inv_data) VALUES ('$last_id', '$jsnVal')";
			
			$tokenId    = base64_encode(random_bytes(32)); //random_bytes    mcrypt_create_iv
			$issuedAt   = time();
			$notBefore  = $issuedAt + 10;                  //Adding 10 seconds
			$expire     = $notBefore + 10 * 24 *60 * 60;  // Adding 60 seconds
			$serverName = "www.mytrailcams.com"; // Retrieve the server name from config file   $config->get('serverName')
			 
			/*
			 * Create the token as an array
			 */
			$data = [
				'iat'  => $issuedAt,         // Issued at: time when the token was generated
				'jti'  => $tokenId,          // Json Token Id: an unique identifier for the token
				'iss'  => $serverName,       // Issuer
				'nbf'  => $notBefore,        // Not before
				'exp'  => $expire,           // Expire
				'data' => [                  // Data related to the signer user
					'userId'   => $last_id, // userid from the users table
					'userName' => $username, // User name
				]
			];
			
			$secretKey = base64_decode("I love you vishakha");     //$config->get('jwtKey')
			
			$jwt = JWT::encode(
				$data,      //Data to be encoded in the JWT
				$secretKey, // The signing key
				'HS512'     // Algorithm used to sign the token, see https://tools.ietf.org/html/draft-ietf-jose-json-web-algorithms-40#section-3
				);

			$sql_upd = "UPDATE imgrec_cms_users SET token='$jwt' WHERE id='$last_id'";

			if($conn->query($sql_upd) === TRUE){
				if ($conn->query($sql_subscrition) === TRUE) {
					$unencodedArray = ['msg' => '',
					'jwt' => $jwt,
					'uid' => $last_id];
					echo json_encode($unencodedArray);
				} else{
					$msg = 'Please try after some time. '
					$unencodedArray = ['msg' => $msg];
					echo json_encode($unencodedArray);
				}
			} else{
				$msg = 'Please try after some time. '
				$unencodedArray = ['msg' => $msg];
				echo json_encode($unencodedArray);
			}
		    //echo "New record created successfully";
		} else {
			$msg = 'Please try after some time. '
			$unencodedArray = ['msg' => $msg];
			echo json_encode($unencodedArray);
		    //echo "Error: " . $sql . "<br>" . $conn->error;
		}
	}

	
}
else
{
	echo "Please Enter all values";
}

function getRealIpAddr()
{
    if (!empty($_SERVER['HTTP_CLIENT_IP']))   //check ip from share internet
    {
      $ip=$_SERVER['HTTP_CLIENT_IP'];
    }
    elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))   //to check ip is pass from proxy
    {
      $ip=$_SERVER['HTTP_X_FORWARDED_FOR'];
    }
    else
    {
      $ip=$_SERVER['REMOTE_ADDR'];
    }
    return $ip;
}



?>
