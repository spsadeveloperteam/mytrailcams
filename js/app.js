var app = angular.module('mytrailcam', []);
app.config(function($routeProvider) {
  $routeProvider
    .when('/', {
      templateUrl: 'partial/properties.html'
    })

    .when('/addnewproperty', {
      templateUrl: 'partial/addnewproperties.html'
    })

    .when('/newproperty', {
      templateUrl: 'partial/newproperties.html'
    })

    .when('/addnewcam', {
      templateUrl: 'partial/addnewcamera.html'
    })

    .when('/uploadphotostocam', {
      templateUrl: 'partial/uploadphotostocam.html'
    })

    .when('/afteraddedprop', {
      templateUrl: 'partial/afteraddedprop.html'
    })

    .when('/uploadphotostocam', {
      templateUrl: 'partial/uploadphotostocam.html'
    })

    .when('/editprop', {
      templateUrl: 'partial/editprop.html'
    })

    .when('/settings', {
      templateUrl: 'partial/settings.html'
    })

    .when('/usrprofile', {
      templateUrl: 'partial/usrprofile.html'
    })

    .when('/usrsettings', {
      templateUrl: 'partial/usrsettings.html'
    })

    .when('/plans', {
      templateUrl: 'partial/plans.html'
    })

    .when('/cancelaccount', {
      templateUrl: 'partial/cancelaccount.html'
    })

    .otherwise({
      redirectTo: '/afteraddedprop'
    });
});


// angular.module('mytrailcam', ['ui.router'])  //,'mytrailcam.controllers'
//      .config(function($stateProvider, $urlRouterProvider){
 
//          //$urlRouterProvider.otherwise('/');

//            $urlRouterProvider.otherwise(function($injector, $location){
//             $injector.invoke(['$state', function($state) {
//                 $state.go('app');
//             }]);
//           });
 
//           $stateProvider
//             .state('app',{
//               url: '/',
//               views: {
//                        '': {
//                           templateUrl: 'partial/properties.html'
//                         }
//                       }
//               })
//             .state('addnewproperty',{
//               url: '/',
//               views: {
//                        '': {
//                           templateUrl: 'partial/addnewproperties.html'
//                          }                        
//                       }
//               })
//             .state('newproperty',{
//               url: '/',
//               views: {
//                        '': {
//                           templateUrl: 'partial/newproperties.html'
//                          }                        
//                       }
//               })
//             .state('addnewcam',{
//               url: '/',
//               views: {
//                        '': {
//                           templateUrl: 'partial/addnewcamera.html'
//                          }                        
//                       }
//               })
//             .state('uploadphotostocam',{
//               url: '/',
//               views: {
//                        '': {
//                           templateUrl: 'partial/uploadphotostocam.html'
//                          }                        
//                       }
//               })
//             .state('afteraddedprop',{
//               url: '/',
//               views: {
//                        '': {
//                           templateUrl: 'partial/afteraddedprop.html'
//                          }                        
//                       }
//               })
//             .state('editprop',{
//               url: '/',
//               views: {
//                        '': {
//                           templateUrl: 'partial/editprop.html'
//                          }                        
//                       }
//               })
 
// });