var app=angular.module('mytrailcam.controllers', [])

app.controller('controller_camera',['$scope','$rootScope','$state','$http','$window',function($scope, $rootScope, $state, $http, $window){

	$scope.upload = true;
	$scope.download = true;
	$scope.discard = true;
	$scope.exit = true;
    
    $scope.exits = function(){
    	$state.go('app');
    }
    
  }]);