var app = angular.module('mytrailcamf', []);
app.config(function($routeProvider) {
  $routeProvider
    .when('/camera', {
      templateUrl: 'partial/camera.html'
    })

    .when('/addnewcam', {
      templateUrl: 'partial/addnewcamera.html'
    })

    .when('/uploadphotostocam', {
      templateUrl: 'partial/uploadphotostocam.html'
    })

    .when('/editcam', {
      templateUrl: 'partial/editcam.html'
    })

    .when('/cameradesc', {
      templateUrl: 'partial/cameradesc.html'
    })

    .when('/photos', {
      templateUrl: 'partial/photos.html'
    })

    .when('/profile', {
      templateUrl: 'partial/profile.html'
    })

    .when('/editprofile', {
      templateUrl: 'partial/editprofile.html'
    })

    .when('/addprofile', {
      templateUrl: 'partial/addprofile.html'
    })

    .when('/profiles', {
      templateUrl: 'partial/savedprofile.html'
    })

    .when('/profiledesc', {
      templateUrl: 'partial/profiledesc.html'
    })

    .when('/album', {
      templateUrl: 'partial/album.html'
    })

    .when('/addalbum', {
      templateUrl: 'partial/addalbum.html'
    })

    .when('/albums', {
      templateUrl: 'partial/albums.html'
    })

    .when('/editalbum', {
      templateUrl: 'partial/editalbum.html'
    })

    .when('/tags', {
      templateUrl: 'partial/tags.html'
    })

    .when('/settings', {
      templateUrl: 'partial/settings.html'
    })

    .when('/usrprofile', {
      templateUrl: 'partial/usrprofile.html'
    })

    .when('/usrsettings', {
      templateUrl: 'partial/usrsettings.html'
    })

    .when('/plans', {
      templateUrl: 'partial/plans.html'
    })

    .when('/cancelaccount', {
      templateUrl: 'partial/cancelaccount.html'
    })
    .when('/photodesc', {
      templateUrl: 'partial/photodesc.html'
    })
    .when('/selalbum', {
      templateUrl: 'partial/selectedalbum.html'
    })

    .otherwise({
      redirectTo: '/camera'
    });
});

// angular.module('mytrailcamf', ['ui.router'])  //,'mytrailcam.controllers'
//      .config(function($stateProvider, $urlRouterProvider){
 
//          $urlRouterProvider.otherwise('/camera');

//           //  $urlRouterProvider.otherwise(function($injector, $location){
//           //   $injector.invoke(['$state', function($state) {
//           //       $state.go('app');
//           //   }]);
//           // });
 
//           $stateProvider
//             .state('camera',{
//               url: '/camera',
//               views: {
//                        '': {
//                           templateUrl: 'partial/camera.html'
//                         }
//                       }
//               })
//             .state('addnewcam',{
//               url: '/addnewcam',
//               views: {
//                        '': {
//                           templateUrl: 'partial/addnewcamera.html'
//                          }                        
//                       }
//               })
//             .state('uploadphotostocam',{
//               url: '/uploadphotostocam',
//               views: {
//                        '': {
//                           templateUrl: 'partial/uploadphotostocam.html'
//                          }                        
//                       }
//               })
//             .state('editcam',{
//               url: '/editcam',
//               views: {
//                        '': {
//                           templateUrl: 'partial/editcam.html'
//                          }                        
//                       }
//               })
            
 
// });